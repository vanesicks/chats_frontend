import React from 'react'
import Loader from "../loader/loader";
import EmployeeRow from "../employeeRow/employeeRow";

const Staff = () => {
    const users = [
        {id: 1, fullName: 'Карпинский Иван', position: 'Администратор', birthdayDate: '30.06.1999', isActive: true}
    ];

    console.log('users: ', users);
    return (
        <div>
            <div>
                <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 overflow-x-auto">
                    <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                        <table className="min-w-full leading-normal">
                            <thead>
                            <tr>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Имя
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Должность
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Дата рождения
                                </th>
                                {/*<th*/}
                                {/*    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">*/}
                                {/*    QRT*/}
                                {/*</th>*/}
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Действия
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {users.map((user)=>(
                                <EmployeeRow key={user.id} user={user} />
                            ))}
                            </tbody>
                        </table>
                        {/*<div*/}
                        {/*    className="px-5 py-5 bg-white border-t flex flex-col xs:flex-row items-center xs:justify-between          ">*/}
						{/*<span className="text-xs xs:text-sm text-gray-900">*/}
                        {/*    Showing 1 to 4 of 50 Entries*/}
                        {/*</span>*/}
                        {/*    <div className="inline-flex mt-2 xs:mt-0">*/}
                        {/*        <button*/}
                        {/*            className="text-sm text-indigo-50 transition duration-150 hover:bg-indigo-500 bg-indigo-600 font-semibold py-2 px-4 rounded-l">*/}
                        {/*            Prev*/}
                        {/*        </button>*/}
                        {/*        &nbsp; &nbsp;*/}
                        {/*        <button*/}
                        {/*            className="text-sm text-indigo-50 transition duration-150 hover:bg-indigo-500 bg-indigo-600 font-semibold py-2 px-4 rounded-r">*/}
                        {/*            Next*/}
                        {/*        </button>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </div>
        </div>
)
}


export default Staff