import React from 'react'

const Settings = () => {
    return (
        <div className="w-full flex">

            <form id="form" className="bg-white w-full rounded px-8 pt-6 pb-8 mb-4">
                    <h1 className="block text-gray-700 font-bold mb-2 text-xl">Настройки</h1>
                    <br/>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
                                Имя
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                name="name" id="name" type="text" placeholder="Введите имя" required/>
                        </div>

                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
                                Фамилия
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                name="tel" id="tel" type="tel" placeholder="Введите фамилию" required/>
                        </div>

                        {/*<div className="mb-4">*/}
                        {/*    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">*/}
                        {/*        Должность*/}
                        {/*    </label>*/}
                        {/*    <input*/}
                        {/*        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"*/}
                        {/*        name="email" id="email" type="email" placeholder="Введите должность" required/>*/}
                        {/*</div>*/}

                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Date">
                                Дата рождения
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                name="date" id="date" type="date" placeholder="Введите дату рождения" required/>
                        </div>

                        <div className="flex items-center justify-between">
                            <button id="submit"
                                    className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                    type="submit">
                                Сохранить
                            </button>
                        </div>
            </form>

        </div>
    )
}


export default Settings