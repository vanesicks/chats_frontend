import React from 'react'

const Header = () => {


    return (
        <div className="bg-black">
            <nav className="flex justify-between p-8 items-center">
                <a href="#" className="font-sans text-black md:text-white text-sm md:text-4xl font-thin">Ayup
                    entertainment</a>
                <ul className="list-reset flex">
                    <li><a href="#" className="text-black md:text-white p-2 md:p-4">Чаты</a></li>
                    <li><a href="#" className="text-black md:text-white p-2 md:p-4 whitespace-no-wrap">Настройки</a></li>
                    <li><a href="#" className="text-black md:text-white p-2 md:p-4">Выйти</a></li>
                </ul>
            </nav>
        </div>
    )
}

export default Header