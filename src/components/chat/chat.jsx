import React, {useEffect} from 'react'
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import * as Yup from 'yup'

import {updateEmailField, updatePasswordField, loginFunction, login} from '../../redux/reducers/auth'
import {useParams} from "react-router-dom";
import {getChatInfo} from "../../redux/reducers/chats";
import ChatsList from "../chatsList/chatsList";
import MessagesList from "../messagesList/messagesList";

const Chat = () => {
    const dispatch = useDispatch();
    // const chats = [
    //     {}
    // ];
    const {chatId} = useParams();
    const chats = useSelector((s) => s.chats);
    console.log(chats);


    return (
        <>
            <ChatsList/>
            {(chatId) && <MessagesList/>}
        </>
    )
}


export default Chat