import { takeLatest, put, select, call } from "redux-saga/effects";
import axios from "axios";
import Cookies from 'universal-cookie'
import {GET_ALL_CHATS, GET_CHAT_INFO, updateAllChats, updateChatInfo} from "../reducers/chats";
import {ERROR_MESSAGE} from "../reducers/auth";

const getChatsState = state => state.chats;
const getAuthState = state => state.auth;

const getChatInfoImitation = (chatId) => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            resolve({
                id: chatId,
                name: 'testChat'+chatId,
                messages: [{id:1,text:'message1',sender: 'sender1'},{id:2,text:'message2', sender: 'sender2'}]
            });
        }, 500);
    });
}
const getAllChatsImitation = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(
                [{
                id: 1,
                name: 'testChat1',
            },{
                id: 2,
                name: 'testChat2',
            },{
                id: 3,
                name: 'testChat3',
            }]);
        }, 300);
    });
}

export function* getChatInfo(action) {
    const authState = yield select(getAuthState);
    if (authState.isLoggedIn) {
        try {
            const response = yield call(getChatInfoImitation, action.chatId)

            yield put(updateChatInfo(response));
        } catch (err) {
            yield put({ type: ERROR_MESSAGE });
        }
    } else {
        yield put({ type: ERROR_MESSAGE });
    }
}
export function* getAllChats(action) {
    const authState = yield select(getAuthState);
    if (authState.isLoggedIn) {
        try {
            const response = yield call(getAllChatsImitation)
            yield put(updateAllChats(response));
        } catch (err) {
            yield put({ type: ERROR_MESSAGE });
        }
    } else {
        yield put({ type: ERROR_MESSAGE });
    }
}



export default function* chatsWatcher() {
    yield takeLatest(GET_CHAT_INFO, getChatInfo);
    yield takeLatest(GET_ALL_CHATS, getAllChats);
}