import { takeLatest, put, select, call } from "redux-saga/effects";
import {LOGIN, loginSuccess, ERROR_MESSAGE } from "../reducers/auth";
import axios from "axios";
import Cookies from 'universal-cookie'

const cookies = new Cookies();
// export function loginFunction() {
//     return (dispatch, getState) => {
//         const { email, password } = getState().auth
//         axios
//             .post(
//                 '/api/v1/auth',
//                 JSON.stringify({
//                     email,
//                     password
//                 }),
//                 {
//                     headers: {
//                         'Content-Type': 'application/json'
//                     }
//                 }
//             )
//             .then(({ data }) => {
//                 dispatch({ type: LOGIN, token: data.token, user: data.user })
//                 history.push('/chat/general')
//                 createSocket(data.token)
//             })
//             .catch((err) => {
//                 dispatch({ type: ERROR_MESSAGE })
//                 console.log(err)
//             })
//     }
// }

const getAuthState = state => state.auth;

const loginImitation = ()=> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            cookies.set('token','asdsdfsdg');
            resolve({
                token: 'asdsdfsdg',
                user: {name: 'test'}
            });
        }, 300);
    });
}

export function* login(action) {
    const authState = yield select(getAuthState);
    if (authState.password && authState.email) {
        try {
            // const response = call(axios.post(
            //         '/api/v1/auth',
            //         JSON.stringify({
            //             email,
            //             password
            //         }),
            //         {
            //             headers: {
            //                 'Content-Type': 'application/json'
            //             }
            //         }
            //     ))
            const response = yield call(loginImitation)
            yield put(loginSuccess(response));
        } catch (err) {
            yield put({ type: ERROR_MESSAGE });
        }
    } else {
        yield put({ type: ERROR_MESSAGE });
    }
}



export default function* loginWatcher() {
    yield takeLatest(LOGIN, login);
}