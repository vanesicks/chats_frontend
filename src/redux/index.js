import {applyMiddleware, combineReducers, createStore} from "redux";
import auth from "./reducers/auth";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import chats from "./reducers/chats";
const rootReducer = combineReducers({
    auth,
    chats
})

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(rootSaga);

export default store
