// const GET_CHAT_INFO = 'GET_CHAT_INFO'
export const ERROR_MESSAGE = 'ERROR_MESSAGE'
export const GET_CHAT_INFO = 'GET_CHAT_INFO'
export const UPDATE_CHAT_INFO = 'UPDATE_CHAT_INFO'
export const GET_ALL_CHATS = 'GET_ALL_CHATS'
export const UPDATE_ALL_CHATS = 'UPDATE_ALL_CHATS'


const initialState = {
   chats: []
}

const chatsReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CHAT_INFO:
            return {...state,
                chats: state.chats.map(
                    (content, i) => content.id == action.payload.id ? {...action.payload}
                        : content
                )
            }
        case ERROR_MESSAGE: {
            return { ...state, errorMessage: true }
        }
        case UPDATE_ALL_CHATS: {
            return { ...state, chats: [...action.chats]}
        }
        default:
            return state
    }
}



export function updateChatInfo(chatInfo) {
    return { type: UPDATE_CHAT_INFO, payload:{...chatInfo} }
}

export function getAllChats() {
    return { type: GET_ALL_CHATS }
}
export function updateAllChats(chats) {
    return { type: UPDATE_ALL_CHATS, chats }
}

export function getChatInfo(chatId) {
    return { type: GET_CHAT_INFO, chatId}
}



export default chatsReducer