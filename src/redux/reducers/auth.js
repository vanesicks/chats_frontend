import Cookies from 'universal-cookie'

const UPDATE_EMAIL = 'UPDATE_LOGIN'
const UPDATE_PASSWORD = 'UPDATE_PASSWORD'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN = 'LOGIN'
export const ERROR_MESSAGE = 'ERROR_MESSAGE'

const cookies = new Cookies()

const initialState = {
    email: '',
    password: '',
    token: cookies.get('token'),
    isLoggedIn: !!cookies.get('token'),
    user: {},
    errorMessage: false
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_EMAIL: {
            return { ...state, email: action.email }
        }
        case UPDATE_PASSWORD: {
            return { ...state, password: action.password }
        }
        case LOGIN_SUCCESS: {
            return { ...state, token: action.token, password: '', user: action.user, isLoggedIn: action.isLoggedIn }
        }
        case ERROR_MESSAGE: {
            return { ...state, errorMessage: true }
        }
        default:
            return state
    }
}

export function updateEmailField(email) {
    return { type: UPDATE_EMAIL, email }
}

export function updatePasswordField(password) {
    return { type: UPDATE_PASSWORD, password }
}

export function loginSuccess(data) {
    return { type: LOGIN_SUCCESS, token: data.token, user: data.user, isLoggedIn: true }
}

export function login() {
    return { type: LOGIN }
}



export default authReducer