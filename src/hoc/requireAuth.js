import {Navigate, useLocation,} from "react-router-dom";
import {useSelector} from "react-redux";

const RequireAuth = ({children}) => {
    const location = useLocation();
    const auth = useSelector((s) => s.auth)

    if (!auth.token) {
        return <Navigate to="/login" state={{from: location}}/>
    }
    return children;
}

export {RequireAuth}