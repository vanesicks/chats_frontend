import React from 'react'
import Header from "../components/header/header";
import Sidebar from "../components/sidebar/sidebar";

const Layout = ({children}) => {
    // var sideBar = document.getElementById("mobile-nav");
    // var openSidebar = document.getElementById("openSideBar");
    // var closeSidebar = document.getElementById("closeSideBar");
    // sideBar.style.transform = "translateX(-260px)";
    //
    // function sidebarHandler(flag) {
    //     if (flag) {
    //         sideBar.style.transform = "translateX(0px)";
    //         openSidebar.classList.add("hidden");
    //         closeSidebar.classList.remove("hidden");
    //     } else {
    //         sideBar.style.transform = "translateX(-260px)";
    //         closeSidebar.classList.add("hidden");
    //         openSidebar.classList.remove("hidden");
    //     }
    // }
    //
    // var sideBar = document.getElementById("mobile-nav");
    // var openSidebar = document.getElementById("openSideBar");
    // var closeSidebar = document.getElementById("closeSideBar");
    // sideBar.style.transform = "translateX(-260px)";
    //
    // function sidebarHandler(flag) {
    //     if (flag) {
    //         sideBar.style.transform = "translateX(0px)";
    //         openSidebar.classList.add("hidden");
    //         closeSidebar.classList.remove("hidden");
    //     } else {
    //         sideBar.style.transform = "translateX(-260px)";
    //         closeSidebar.classList.add("hidden");
    //         openSidebar.classList.remove("hidden");
    //     }
    // }

    return (
        <>
            <div className="w-full h-full">
                <dh-component>
                    <div className="flex flex-no-wrap">
                        <Sidebar />
                        {/*<div className="container mx-auto py-10 h-64 md:w-4/5 w-11/12 px-6">*/}
                            {/*<div className="w-full h-full rounded border-dashed border-2 border-gray-300">*/}
                                {children}
                            {/*</div>*/}
                        {/*</div>*/}
                    </div>
                </dh-component>
            </div>
        </>
    )
}


export default Layout