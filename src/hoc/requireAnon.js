import {Navigate, useLocation,} from "react-router-dom";
import {useSelector} from "react-redux";

const RequireAnon = ({children}) => {
    const location = useLocation();
    const auth = useSelector((s) => s.auth)

    if (!!auth.token) {
        return <Navigate to="/" state={{from: location}} replace/>
    }
    return children;
}

export {RequireAnon}