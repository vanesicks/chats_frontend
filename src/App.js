import logo from './logo.svg';
import './App.css';
import Header from "./components/header/header";
import {Provider, useDispatch, useSelector} from 'react-redux'
import {ConnectedRouter} from 'connected-react-router'
import {Routes, Route, Redirect, StaticRouter, BrowserRouter, Navigate, useRoutes, useParams} from 'react-router-dom'
import {useEffect} from "react";
import Login from "./components/login/login";
import Error404 from "./components/404/error404";
import Chat from "./components/chat/chat";
import {RequireAuth} from "./hoc/requireAuth";
import Staff from "./components/staff/staff";
import {RequireAnon} from "./hoc/requireAnon";
import Layout from "./hoc/layout";
import Settings from "./components/settings/Settings";
import {getAllChats, getChatInfo} from "./redux/reducers/chats";


function App() {
    const auth = useSelector((s) => s.auth)
    const isLoggedIn = auth.isLoggedIn;
    //
    // const routing = useRoutes(routes(isLoggedIn));
    // console.log(routing);
    // let dispatch = useDispatch();
    // useEffect(()=>{
    //
    // },[]);

    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getAllChats());
    }, []);

    return (
        <div className="container-fluid mx-auto">
            {/*<Header/>*/}
            <Routes>
                <Route path="/" element={
                    <RequireAuth>
                        <Layout>
                            <Staff/>
                        </Layout>
                    </RequireAuth>
                }/>
                <Route path="/staff" element={
                    <RequireAuth>
                        <Layout>
                            <Staff/>
                        </Layout>
                    </RequireAuth>
                }/>
                <Route path="/chat" element={
                    <RequireAuth>
                        <Layout>
                            <Chat/>
                        </Layout>
                    </RequireAuth>
                }/>
                <Route path="/chat/:chatId" element={
                    <RequireAuth>
                        <Layout>
                            <Chat/>
                        </Layout>
                    </RequireAuth>
                }/>
                <Route path="/settings" element={
                    <RequireAuth>
                        <Layout>
                            <Settings/>
                        </Layout>
                    </RequireAuth>
                }/>

                <Route path="/login" element={
                    <RequireAnon>
                        <Login/>
                    </RequireAnon>
                }/>
                <Route path="/registration" element={
                    <RequireAnon>
                        <Login/>
                    </RequireAnon>
                }/>
                <Route path="*" element={<Error404/>}/>
            </Routes>
        </div>
    );
}

export default App;
